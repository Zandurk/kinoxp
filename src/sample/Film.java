package sample;

/**
 * Created by Alexander on 27-10-2015.
 */
public class Film {
int[][] tidDatoArray;
String titel;

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public int[][] getTidDatoArray() {
        return tidDatoArray;
    }

    public void setTidDatoArray(int[][] tidDatoArray) {
        this.tidDatoArray = tidDatoArray;
    }
}
