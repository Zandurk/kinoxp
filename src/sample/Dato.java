package sample;

import java.sql.Timestamp;
import java.util.GregorianCalendar;

/**
 * Created by Alexander on 27-10-2015.
 */
public class Dato {

    Timestamp dato;

    public Timestamp getDato() {
        return dato;
    }

    public void setDato(Timestamp dato) {
        this.dato = dato;
    }

    public int getCurrentDate(){
        GregorianCalendar gc = new GregorianCalendar();
       return gc.DAY_OF_WEEK;

    }
}
